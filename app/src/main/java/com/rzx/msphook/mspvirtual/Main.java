package com.rzx.msphook.mspvirtual;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.text.TextUtils;
import android.webkit.WebView;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

@SuppressLint("NewApi")
public class Main implements IXposedHookLoadPackage {

    // private String packageListPath = "mnt/sdcard/msp/res/packageList.txt";

    @Override
    public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {
        // TODO Auto-generated method stub
        if (!isHookPackage(lpparam.packageName))
            return;
        setvirtualDeviceInfo(lpparam.classLoader);
        setLocationInfo(lpparam.classLoader);
        enableWebviewDebug(lpparam.classLoader);
    }

    private void enableWebviewDebug(ClassLoader classLoader) {
        // 勾住 WebView 所有的构造器
        XposedBridge.hookAllConstructors(WebView.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                // 打开webContentsDebuggingEnabled
                XposedHelpers.callStaticMethod(WebView.class, "setWebContentsDebuggingEnabled", true);
            }
        });

        // 避免手动设置为false情况
        XposedHelpers.findAndHookMethod(WebView.class.getName()
                , classLoader
                , "setWebContentsDebuggingEnabled"
                , boolean.class
                , new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                        param.args[0] = true;
                    }
                });
    }

    private boolean isHookPackage(String packageName) {
        return getPackageName().contains(packageName);

    }

    private List<String> getPackageName() {
        List<String> list = new ArrayList<>();
        list.add("com.tencent.mm");
        list.add("com.facebook.katana");
        list.add("com.rzx.mspvirtual");
        list.add("com.tencent.map");
        list.add("com.rzx.appmator");

        // try {
        //
        // FileInputStream inputStream = new FileInputStream(packageListPath);
        // BufferedReader bufferedReader = new BufferedReader(
        // new InputStreamReader(inputStream));
        //
        // String str = null;
        // while ((str = bufferedReader.readLine()) != null) {
        // list.add(str);
        // }
        //
        // // close
        // inputStream.close();
        // bufferedReader.close();
        //
        // } catch (FileNotFoundException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }

        return list;
    }

    private void setvirtualDeviceInfo(ClassLoader classLoader) {
        XposedHelpers.findAndHookMethod(
                android.telephony.TelephonyManager.class.getName(),
                classLoader, "getDeviceId", StringMethodHook("imei"));
        XposedHelpers.findAndHookMethod(
                "com.android.internal.telephony.PhoneSubInfo", classLoader,
                "getDeviceId", StringMethodHook("imei"));
        XposedHelpers.findAndHookMethod(
                android.telephony.TelephonyManager.class.getName(),
                classLoader, "getSubscriberId", StringMethodHook("imsi"));

        XposedHelpers.findAndHookMethod(
                android.net.wifi.WifiInfo.class.getName(), classLoader,
                "getMacAddress", StringMethodHook("mac"));

    }

    private Object StringMethodHook(final String key) {
        // TODO Auto-generated method stub
        XC_MethodHook methodHook = new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param)
                    throws Throwable {
                // TODO Auto-generated method stub
                super.afterHookedMethod(param);
                String value = InitParam.getDeviceValue(key);
                Object result = param.getResult();
                if (result == null)
                    return;
                if (!TextUtils.isEmpty(value)) {
                    if (value.equals("null"))
                        value = null;
                    param.setResult(value);
                }
            }
        };

        return methodHook;
    }

    private void setLocationInfo(ClassLoader classLoader) {
        if (TextUtils.isEmpty(InitParam.getLocationValue("lat")))
            return;

        setGps(classLoader);
        setCell(classLoader);
        // String bssid = "b8:e4:cf:d0:43:3d";
        // String ssid = "360wifi";
        setWifi(classLoader, "", "");
    }

    private void setGps(ClassLoader classLoader) {

        XposedHelpers.findAndHookMethod(Location.class.getName(), classLoader,
                "getLatitude", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        double lat = Double.parseDouble(InitParam
                                .getLocationValue("lat"));
                        param.setResult(lat);
                    }
                });
        XposedHelpers.findAndHookMethod(Location.class.getName(), classLoader,
                "getLongitude", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        double lng = Double.parseDouble(InitParam
                                .getLocationValue("lng"));
                        param.setResult(lng);
                    }
                });

        XposedHelpers.findAndHookMethod(LocationManager.class,
                "getLastLocation", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {

                        double lat = Double.parseDouble(InitParam
                                .getLocationValue("lat"));
                        double lng = Double.parseDouble(InitParam
                                .getLocationValue("lng"));
                        Location l = new Location(LocationManager.GPS_PROVIDER);
                        l.setLatitude(lat);
                        l.setLongitude(lng);
                        l.setAccuracy(100f);
                        l.setTime(System.currentTimeMillis());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            l.setElapsedRealtimeNanos(SystemClock
                                    .elapsedRealtimeNanos());
                        }
                        param.setResult(l);
                    }
                });

        XposedHelpers.findAndHookMethod(LocationManager.class,
                "getLastKnownLocation", String.class, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        Location l = new Location(LocationManager.GPS_PROVIDER);

                        double lat = Double.parseDouble(InitParam
                                .getLocationValue("lat"));
                        double lng = Double.parseDouble(InitParam
                                .getLocationValue("lng"));
                        l.setLatitude(lat);
                        l.setLongitude(lng);
                        l.setAccuracy(100f);
                        l.setTime(System.currentTimeMillis());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            l.setElapsedRealtimeNanos(SystemClock
                                    .elapsedRealtimeNanos());
                        }
                        param.setResult(l);
                    }
                });

        XposedBridge.hookAllMethods(LocationManager.class, "getProviders",
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        ArrayList<String> arrayList = new ArrayList<>();
                        arrayList.add("gps");
                        param.setResult(arrayList);
                    }
                });

        XposedHelpers.findAndHookMethod(LocationManager.class,
                "getBestProvider", Criteria.class, Boolean.TYPE,
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        param.setResult("gps");
                    }
                });

        XposedHelpers.findAndHookMethod(LocationManager.class,
                "addGpsStatusListener", GpsStatus.Listener.class,
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        if (param.args[0] != null) {
                            XposedHelpers.callMethod(param.args[0],
                                    "onGpsStatusChanged", 1);
                            XposedHelpers.callMethod(param.args[0],
                                    "onGpsStatusChanged", 3);
                        }
                    }
                });

        XposedHelpers.findAndHookMethod(LocationManager.class,
                "addNmeaListener", GpsStatus.NmeaListener.class,
                new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param)
                            throws Throwable {
                        param.setResult(false);
                    }
                });

        XposedHelpers.findAndHookMethod("android.location.LocationManager",
                classLoader, "getGpsStatus", GpsStatus.class,
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        GpsStatus gss = (GpsStatus) param.getResult();
                        if (gss == null)
                            return;

                        Class<?> clazz = GpsStatus.class;
                        Method m = null;
                        for (Method method : clazz.getDeclaredMethods()) {
                            if (method.getName().equals("setStatus")) {
                                if (method.getParameterTypes().length > 1) {
                                    m = method;
                                    break;
                                }
                            }
                        }
                        if (m == null)
                            return;

                        // access the private setStatus function of GpsStatus
                        m.setAccessible(true);

                        // make the apps belive GPS works fine now
                        int svCount = 5;
                        int[] prns = {1, 2, 3, 4, 5};
                        float[] snrs = {0, 0, 0, 0, 0};
                        float[] elevations = {0, 0, 0, 0, 0};
                        float[] azimuths = {0, 0, 0, 0, 0};
                        int ephemerisMask = 0x1f;
                        int almanacMask = 0x1f;

                        // 5 satellites are fixed
                        int usedInFixMask = 0x1f;

                        XposedHelpers.callMethod(gss, "setStatus", svCount,
                                prns, snrs, elevations, azimuths,
                                ephemerisMask, almanacMask, usedInFixMask);
                        param.args[0] = gss;
                        param.setResult(gss);
                        try {
                            m.invoke(gss, svCount, prns, snrs, elevations,
                                    azimuths, ephemerisMask, almanacMask,
                                    usedInFixMask);
                            param.setResult(gss);
                        } catch (Exception e) {
                            XposedBridge.log(e);
                        }
                    }
                });

        for (Method method : LocationManager.class.getDeclaredMethods()) {
            if (method.getName().equals("requestLocationUpdates")
                    && !Modifier.isAbstract(method.getModifiers())
                    && Modifier.isPublic(method.getModifiers())) {
                XposedBridge.hookMethod(method, new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param)
                            throws Throwable {
                        if (param.args.length >= 4
                                && (param.args[3] instanceof LocationListener)) {

                            LocationListener ll = (LocationListener) param.args[3];

                            Class<?> clazz = LocationListener.class;
                            Method m = null;
                            for (Method method : clazz.getDeclaredMethods()) {
                                if (method.getName()
                                        .equals("onLocationChanged")
                                        && !Modifier.isAbstract(method
                                        .getModifiers())) {
                                    m = method;
                                    break;
                                }
                            }
                            Location l = new Location(
                                    LocationManager.GPS_PROVIDER);
                            double lat = Double.parseDouble(InitParam
                                    .getLocationValue("lat"));
                            double lng = Double.parseDouble(InitParam
                                    .getLocationValue("lng"));
                            l.setLatitude(lat);
                            l.setLongitude(lng);
                            l.setAccuracy(10.00f);
                            l.setTime(System.currentTimeMillis());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                l.setElapsedRealtimeNanos(SystemClock
                                        .elapsedRealtimeNanos());
                            }
                            XposedHelpers
                                    .callMethod(ll, "onLocationChanged", l);
                            try {
                                if (m != null) {
                                    m.invoke(ll, l);
                                }
                            } catch (Exception e) {
                                XposedBridge.log(e);
                            }
                        }
                    }
                });
            }

            if (method.getName().equals("requestSingleUpdate ")
                    && !Modifier.isAbstract(method.getModifiers())
                    && Modifier.isPublic(method.getModifiers())) {
                XposedBridge.hookMethod(method, new XC_MethodHook() {
                    @Override
                    protected void beforeHookedMethod(MethodHookParam param)
                            throws Throwable {
                        if (param.args.length >= 3
                                && (param.args[1] instanceof LocationListener)) {

                            LocationListener ll = (LocationListener) param.args[3];

                            Class<?> clazz = LocationListener.class;
                            Method m = null;
                            for (Method method : clazz.getDeclaredMethods()) {
                                if (method.getName()
                                        .equals("onLocationChanged")
                                        && !Modifier.isAbstract(method
                                        .getModifiers())) {
                                    m = method;
                                    break;
                                }
                            }

                            try {
                                if (m != null) {
                                    Location l = new Location(
                                            LocationManager.GPS_PROVIDER);
                                    double lat = Double.parseDouble(InitParam
                                            .getLocationValue("lat"));
                                    double lng = Double.parseDouble(InitParam
                                            .getLocationValue("lng"));
                                    l.setLatitude(lat);
                                    l.setLongitude(lng);
                                    l.setAccuracy(100f);
                                    l.setTime(System.currentTimeMillis());
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                        l.setElapsedRealtimeNanos(SystemClock
                                                .elapsedRealtimeNanos());
                                    }
                                    m.invoke(ll, l);
                                }
                            } catch (Exception e) {
                                XposedBridge.log(e);
                            }
                        }
                    }
                });
            }
        }
    }

    // 基站
    private void setCell(ClassLoader classLoader) {

        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager",
                classLoader, "getCellLocation", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {

                        param.setResult(null);
                    }
                });

        XposedHelpers.findAndHookMethod("android.telephony.PhoneStateListener",
                classLoader, "onCellLocationChanged", CellLocation.class,
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        param.setResult(null);
                    }
                });

        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager",
                classLoader, "listen", PhoneStateListener.class, int.class,
                new XC_MethodHook() {
                    protected void beforeHookedMethod(MethodHookParam param)
                            throws Throwable {
                        int value = (Integer) param.args[1];
                        if ((value & 0x10) != 0) {
                            param.args[1] = value - 0x10;
                        }

                    }

                });

        XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager",
                classLoader, "getNeighboringCellInfo", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        param.setResult(new ArrayList<>());
                    }
                });

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            XposedHelpers.findAndHookMethod(
                    "android.telephony.TelephonyManager", classLoader,
                    "getAllCellInfo", new XC_MethodHook() {
                        @Override
                        protected void afterHookedMethod(MethodHookParam param)
                                throws Throwable {
                            param.setResult(new ArrayList<>());
                        }
                    });

        }
    }

    // wifi
    private void setWifi(ClassLoader classLoader, final String bssid,
                         final String ssid) {

        // XposedHelpers.findAndHookMethod(
        // android.net.wifi.WifiInfo.class.getName(), classLoader,
        // "getBSSID", new XC_MethodHook() {
        // @Override
        // protected void afterHookedMethod(MethodHookParam param)
        // throws Throwable {
        // param.setResult(bssid);
        // }
        // });
        // XposedHelpers.findAndHookMethod(
        // android.net.wifi.WifiInfo.class.getName(), classLoader,
        // "getSSID", new XC_MethodHook() {
        // @Override
        // protected void afterHookedMethod(MethodHookParam param)
        // throws Throwable {
        // param.setResult(ssid);
        // }
        // });

        XposedHelpers.findAndHookMethod(
                android.net.wifi.WifiManager.class.getName(), classLoader,
                "getScanResults", new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param)
                            throws Throwable {
                        super.afterHookedMethod(param);

                        param.setResult(new ArrayList<>());

                    }
                });

    }
}
