package com.rzx.msphook.mspvirtual;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import android.annotation.SuppressLint;

@SuppressLint("NewApi")
public class InitParam {
	static String locationPath = "mnt/sdcard/msp/res/location.properties";
	static String virtualDevicePath = "mnt/sdcard/msp/res/virtualDeviceInfo.properties";

	private static String getValue(String path, String key) {
		try {
			Properties pps = new Properties();
			FileInputStream inputStream = new FileInputStream(path);
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					inputStream, "utf-8"));
			pps.load(bf);
			inputStream.close();
			return pps.getProperty(key);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String getLocationValue(String key) {

		return getValue(locationPath, key);
	}

	public static String getDeviceValue(String key) {

		return getValue(virtualDevicePath, key);
	}

}
