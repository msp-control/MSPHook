package com.rzx.msphook.util;

/**
 * Created by Administrator on 2017/2/4.
 */
public class Agent {

	public static final String VERSION = "1.0.17";

	public static void main(String[] args) {
		for (String arg : args) {
			if (arg.equals("--version")) {
				System.out.println(VERSION);
			}
		}
	}
}
